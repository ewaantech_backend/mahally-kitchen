import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

import { ApiService } from './services/api.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './views/auth/login/login.component';
import { ForgotPasswordComponent } from './views/auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './views/auth/reset-password/reset-password.component';
import { VerifyComponent } from './views/auth/verify/verify.component';
import { VerifyResendComponent } from './views/auth/verify-resend/verify-resend.component';
import { HeaderComponent } from './views/component/header/header.component';
import { FooterComponent } from './views/component/footer/footer.component';
import { OrderComponent } from './views/pages/order/order.component';
import { OrderReadyForPickupComponent } from './views/pages/order-ready-for-pickup/order-ready-for-pickup.component';
import { OrderDeliveredComponent } from './views/pages/order-delivered/order-delivered.component';
import { BumpComponent } from './views/bump/bump/bump.component';
import { BumpOrderConfirmationComponent } from './views/bump/bump-order-confirmation/bump-order-confirmation.component';
import { BumpKitchenComponent } from './views/bump/bump-kitchen/bump-kitchen.component';
import { ControlMessagesComponent } from './views/component/control-messages/control-messages.component';
import { JwtInterceptor } from './utils/jwt.interceptor';
// import { TokenInterceptor} from './utils/token.interceptor';
import { AuthGuard } from './guards/auth.guard';
import { NotfoundComponent } from './views/notfound/notfound.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { Ng2CompleterModule } from 'ng2-completer';
import {NgxPaginationModule} from 'ngx-pagination';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ChangePasswordComponent } from './views/auth/change-password/change-password.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    VerifyComponent,
    VerifyResendComponent,
    HeaderComponent,
    FooterComponent,
    OrderComponent,
    OrderReadyForPickupComponent,
    OrderDeliveredComponent,
    BumpComponent,
    BumpOrderConfirmationComponent,
    BumpKitchenComponent,
    ControlMessagesComponent,
    NotfoundComponent,
    ChangePasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule ,
    ToastrModule.forRoot(),
    Ng2CompleterModule,
    NgxPaginationModule
  ],
  providers: [AuthGuard,ApiService,{
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptor,
    multi: true
  },{provide: LocationStrategy, useClass: HashLocationStrategy},
  JwtHelperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
