import { Component, OnInit, Inject } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import {DOCUMENT} from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
declare var jQuery:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  role_id:any;
  ln:any;
  langindex:any=0;
  rest_name:any;
   private routerInfo: BehaviorSubject<string>;

  constructor(
    @Inject(DOCUMENT) private document,
    private router: Router,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { 
    
    this.routerInfo = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.rest_name = localStorage.getItem('restuarants_name_en');
  
  }

  ngOnInit(): void {
    jQuery(".menu-btn").click(function () {
      jQuery(this).toggleClass('active');
      jQuery("body").toggleClass('overflow-hidden');
      jQuery('#main-navigation').toggleClass('open');
      jQuery("#main-navigation ul").toggle();
      return false;
    });
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.ln=sessionStorage.getItem('ln');
    console.log(this.ln);
    if (this.ln === null){
   
      sessionStorage.setItem('ln','en');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
      this.rest_name = localStorage.getItem('restuarants_name_en');
    }
    else if(this.ln === 'ar'){
      
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css');
      this.rest_name = localStorage.getItem('restuarants_name_ar');
    }
    else if(this.ln === 'en'){
  
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
      this.rest_name = localStorage.getItem('restuarants_name_en');
    }
}

  setValue(ln: any): void {
    this.routerInfo.next(ln);
  }

  // public get getLanguage(): string {
  //   return this.language.value;
  // }

  //Language Change
  doLangaugeChange(ln: any){
    if (ln === 'ar'){
      
      sessionStorage.setItem('ln','ar');
      sessionStorage.setItem('langIndex','1');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css'); 
      this.rest_name = localStorage.getItem('restuarants_name_ar');
      location.reload();
    }
    else if (ln === 'en'){
   
      sessionStorage.setItem('ln','en');
      sessionStorage.setItem('langIndex','0');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
      this.rest_name = localStorage.getItem('restuarants_name_en');
      location.reload();
    }
    
  }
    //Logout function

    doLogout() {
      this.apiService.doLogout()
        .subscribe(
          (data: any) => {
            localStorage.clear();
            this.router.navigate(['/login']);
            this.toastr.success(this.langindex==0?'Logout Successfull':'تم تسجيل الخروج بنجاح');
          },
          error => {
            this.toastr.error(error.error.error.msg);
            
          }
        );
    }
    dochangepassword() {

      
      console.log(localStorage.getItem('id'));
            this.router.navigate(['/reset-password'],{ queryParams: { id: localStorage.getItem('id')}});
           
          }
}
