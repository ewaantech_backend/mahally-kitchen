import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { Ng2CompleterModule } from 'ng2-completer';
import { JwPaginationComponent } from 'jw-angular-pagination';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  order_status:number;
  messageType = null;
  orderList = [];
  Loaddata=[];
  searchForm: FormGroup;
  isInitialLogin:boolean;
  update_status: number;
  isAddons: any;
  AutoFillData=[];
  CurrentFilterValue:any;
  currentPage:any;
  totalItems:any;
  OrderitemsPerPage:number=10;
  TimerCount:number=10;
  doReload:boolean=true;
  loadMoreCount:number=0;
  langindex:any=0;
  isFIlterOn:any=0;
  
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.searchForm = this.formBuilder.group({
      order_id: ['', [Validators.required]]
    });
    this.GetConfirmDetails();
    this.doFetchOrderList();
    this.currentPage=1;
 
    // setTimeout(
    //   function(){ 
    //  // console.log('Reload Called');
    //   location.reload(); 
    //   }, 60000);
   if(this.doReload==true)
   {
    setInterval( () => {
      this.TimerCount=this.TimerCount-1;
     if(this.TimerCount==0&& this.doReload==true)
      {
        this.doFetchOrderList();
        this.TimerCount=10;    
      }
      if(this.doReload==false)
      {
        this.TimerCount=10;   
      }

    },1000);
   
  }
  }
 
ngOnDestroy(): void
{
  this.doReload=false;
}
 
//   onChangePage(pageOfItems: Array<any>) {
//     // update current page of items
//     this.pageOfItems = pageOfItems;
// }


            doloadmore(){

              this.loadMoreCount=this.loadMoreCount+1;

              this.apiService.doGetActiveOrderList()
                  .subscribe(
                    (data: any) => {

                      console.log(this.orderList.length);
                      this.orderList.push.apply(this.orderList,data.data.orders);
                      console.log('loadmore');
                      console.log(this.orderList);
                      
                    }
                  );
              
            }
  GetConfirmDetails() {  
    this.SpinnerService.show();    
    this.apiService.doGetActiveOrderList()
      .subscribe(
        (data: any) => {
          this.orderList =data.data.orders;
          //this.orderList = data; 
          //console.log(this.orderList);
          this.SpinnerService.hide();
        }
      );
  }

  doFetchOrderList() {
    //this.SpinnerService.show(); 
    this.apiService.doGetActiveOrderList()
      .subscribe(
        (data: any) => {
          //console.log(data.data.orders);
          this.orderList =data.data.orders;
          this.AutoFillData=data.data.orders.map(x=>x.order_id);
          this.totalItems=this.AutoFillData.length;
    //this.items = Array(data.data.orders).fill(0);
          //this.toastr.success('Order Fetched');
          
          //this.SpinnerService.hide();
        },
        error => {
          this.toastr.error(error.error.error.msg);
          console.log(error);
          //this.SpinnerService.hide();
        }
      );
      }

      doResetsearch(){
        this.doReload=true;
        this.isFIlterOn=0;
        location.reload();
    
      }
  
      doFetchOrderListByOrderid() {
        this.doReload=false;
        this.isFIlterOn=1;
        this.SpinnerService.show(); 
        let data: any = this.searchForm.value;
        // Linto this.apiService.doGetOrderListByOrderid(data.order_id)
        this.apiService.doGetOrderListByOrderid(this.CurrentFilterValue)
          .subscribe(
            (data: any) => {
              //console.log(data.data.orders);
              this.orderList =data.data.orders;
              this.totalItems=data.data.orders.length;
              this.SpinnerService.hide();
            },
            error => {
              this.toastr.error(error.error.error.msg);
              console.log(error);
              this.SpinnerService.hide();
            }
          );
          }    
          
      doBump(order_id : number,order_status:number){
        this.update_status=3;
        if(order_status==2)
        {
          this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: order_id,order_status:order_status }});
  
        }
        else{
          this.router.navigate(['/bump-kitchen'],{ queryParams: { order_id: order_id,order_status:order_status }});
  
        }
           }   
      
     doViewDetails(order_id : number,order_status:number){
      this.router.navigate(['/bump-order-confirmation'],{ queryParams:  { order_id: order_id,order_status:order_status,redirectoPage:'orders' }});
     } 

     onPageChange(number: number) {
      // this.logEvent(`pageChange(${number})`);
      // this.config.currentPage = number;
      console.log('page index change');
      this.currentPage=number;
  }

}

