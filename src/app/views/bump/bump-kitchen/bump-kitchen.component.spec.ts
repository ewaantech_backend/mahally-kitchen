import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BumpKitchenComponent } from './bump-kitchen.component';

describe('BumpKitchenComponent', () => {
  let component: BumpKitchenComponent;
  let fixture: ComponentFixture<BumpKitchenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BumpKitchenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BumpKitchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
