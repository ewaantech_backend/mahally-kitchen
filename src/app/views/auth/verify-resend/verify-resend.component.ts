import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import {DOCUMENT} from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-verify-resend',
  templateUrl: './verify-resend.component.html',
  styleUrls: ['./verify-resend.component.css']
})
export class VerifyResendComponent implements OnInit {

  message = null;
  messageType = null;
  verifyForm: FormGroup;
  verifyResendForm: FormGroup;
  ln:any;
  isEnglish:boolean;
  langindex:any=0;
  private language: BehaviorSubject<string>;
  private routerInfo: BehaviorSubject<string>;

  constructor(
    @Inject(DOCUMENT) private document,
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) {
    this.language = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.routerInfo = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.isEnglish=true;
   }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.verifyForm = this.formBuilder.group({
      code:['',[Validators.required]]
    });
    this.verifyResendForm = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]]
    });

    this.ln=sessionStorage.getItem('ln');
    console.log(this.ln);
    if (this.ln === null){
      this.isEnglish=true;
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
    }
    else if(this.ln === 'ar'){
      this.isEnglish=true;
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css');
    }
    else if(this.ln === 'en'){
      this.isEnglish=false;
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
    }
  }

  setValue(ln: any): void {
    this.routerInfo.next(ln);
  }

  public get getLanguage(): string {
    return this.language.value;
  }

  doLangaugeChange(ln: any){
    if (ln === 'ar'){
      this.isEnglish=false;
      sessionStorage.setItem('ln','ar');
      sessionStorage.setItem('langIndex','1');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css'); 
    }
    else if (ln === 'en'){
      this.isEnglish=true;
      sessionStorage.setItem('ln','en');
      sessionStorage.setItem('langIndex','0');
      this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
      this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
    }
    this.ln=this.getLanguage;
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
  }
      
  //forgotPassword  verification
  doVerify() {
    let data: any = this.verifyForm.value;
    this.apiService.doVerify(data.email,data.code)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/reset-password'],{ queryParams: { id: data.user.id }});
          console.log(data);
        },
        error => {
          this.verifyForm = this.formBuilder.group({
            code:['',[Validators.required]]
          });
          this.message = error.error.error.msg;
          this.toastr.error(this.message);
        }
      );
  }

  temForgot(){
    this.router.navigate(['/verify']);
    this.toastr.success(this.langindex==0?'Reset link has been sent to your registered email':'تم إرسال رابط إعادة الضبط لبريدك الإلكتروني المسجل');
    this.verifyResendForm = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]]
    });
  }
}

