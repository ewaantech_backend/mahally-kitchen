import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ValidationService } from 'src/app/services/validation.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from "@angular/router";
import { MustMatch } from './../../../utils/mustmatch.service';
import {DOCUMENT} from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  message = null;
  messageType = null;
  resetForm: FormGroup;
  id:any
  ln:any;
  langindex:any=0;
  isEnglish:boolean;
  private language: BehaviorSubject<string>;
  private routerInfo: BehaviorSubject<string>;
  constructor(
    @Inject(DOCUMENT) private document,
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private toastr: ToastrService,
    private route: ActivatedRoute
  ) { 
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      if (this.id !== undefined) {
      }
      else{
      this.router.navigate(['/verify-resend']);
      }
    });
    this.language = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.routerInfo = new BehaviorSubject<string>(sessionStorage.getItem('ln'));
    this.isEnglish=true;
  }

  ngOnInit(): void {
    this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    this.resetForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      c_password: ['', [Validators.required]]
    }, {
        validator: MustMatch('password', 'c_password')
      });

      this.ln=sessionStorage.getItem('ln');
      console.log(this.ln);
      if (this.ln === null){
        this.isEnglish=true;
        this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
        this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
      }
      else if(this.ln === 'ar'){
        this.isEnglish=true;
        this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
        this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css');
      }
      else if(this.ln === 'en'){
        this.isEnglish=false;
        this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
        this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
      }
    }

    setValue(ln: any): void {
      this.routerInfo.next(ln);
    }
  
    public get getLanguage(): string {
      return this.language.value;
    }
  
    doLangaugeChange(ln: any){
      if (ln === 'ar'){
        this.isEnglish=false;
        sessionStorage.setItem('ln','ar');
        this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap-rtl.min.css');
        this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main-rtl.css'); 
      }
      else if (ln === 'en'){
        this.isEnglish=true;
        sessionStorage.setItem('ln','en');
        this.document.getElementById('theme_bs').setAttribute('href', './assets/stylesheets/bootstrap.min.css');
        this.document.getElementById('theme_main').setAttribute('href', './assets/stylesheets/main.css');
      }
      this.ln=this.getLanguage;
      this.langindex=sessionStorage.getItem('langIndex')==null?0:sessionStorage.getItem('langIndex');
    }

  doReset() {
    let data: any = this.resetForm.value;
    this.apiService.doReset(data.password, data.c_password,this.id)
      .subscribe(
        (data: any) => {
          this.router.navigate(['/login']);
          
          this.toastr.success(this.langindex==0?'Password Updated':'تم تحديث كلمة المرور');
        },
        error => {
          this.toastr.error(error.error.error.msg);
        }
      );
  }

}
