import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

//Pages
import { LoginComponent } from './views/auth/login/login.component';
import { ForgotPasswordComponent } from './views/auth/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './views/auth/reset-password/reset-password.component';
import { VerifyComponent } from './views/auth/verify/verify.component';
import { VerifyResendComponent } from './views/auth/verify-resend/verify-resend.component';

import { OrderComponent } from './views/pages/order/order.component';
import { OrderReadyForPickupComponent } from './views/pages/order-ready-for-pickup/order-ready-for-pickup.component';
import { OrderDeliveredComponent } from './views/pages/order-delivered/order-delivered.component';

import { BumpComponent } from './views/bump/bump/bump.component';
import { BumpOrderConfirmationComponent } from './views/bump/bump-order-confirmation/bump-order-confirmation.component';
import { BumpKitchenComponent } from './views/bump/bump-kitchen/bump-kitchen.component';
import { NotfoundComponent } from './views/notfound/notfound.component';
import { ChangePasswordComponent } from './views/auth/change-password/change-password.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'verify',
    component: VerifyComponent
  },
  {
    path: 'verify-resend',
    component: VerifyResendComponent
  },
  {
    path: 'order',
    component: OrderComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'order-ready-for-pickup',
    component: OrderReadyForPickupComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'order-delivered',
    component: OrderDeliveredComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'bump',
    component: BumpComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'bump-order-confirmation',
    component: BumpOrderConfirmationComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'bump-kitchen',
    component: BumpKitchenComponent,
    canActivate:[AuthGuard]
  },
  {
    path: '**',
    component: NotfoundComponent,
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
